import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Brandon Shreve on 8/10/2015.
 */
public class SortingAlgosTest {
    public SortingAlgos classUnderTest = new SortingAlgos();

    @Test
    public void testQuickSort() throws Exception {

    }

    @Test
    public void testMergeSort() throws Exception {
        int[] unsortedArray = {9, 10, 8, 7, 6, 5, 4, 3, 2, 1};

        System.out.println("Unsorted Array\n--------------");
        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.print(unsortedArray[i] + " ");
        }
        System.out.println("\nSorted Array\n--------------");
        classUnderTest.mergeSort(unsortedArray, 0, unsortedArray.length);
        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.print(unsortedArray[i] + " ");
        }
    }
}