import sun.reflect.generics.reflectiveObjects.NotImplementedException;

class Node {
	public Node leftChild;
	public Node rightChild;
	char data;
	
	public Node(char data) {
		this.data = data;
	}
}

public class BinaryTreeTraversal {

	/* PreOrder
	 * root, left, right
	 */
	public void preOrderTraversal(Node root) {
		if (root == null) return;
		
		System.out.println(root.data);
		preOrderTraversal(root.leftChild);
		preOrderTraversal(root.rightChild);
	}
	
	/* InOrder
	 * left, root, right
	 */
	public void inOrderTraversal(Node root) {
		if (root == null) return;
		
		inOrderTraversal(root.leftChild);
		System.out.println(root.data);
		inOrderTraversal(root.rightChild);
	}
	
	/* PostOrder
	 * left, right, root
	 */
	public void postOrderTraversal(Node root) {
		if (root == null) return;
		
		postOrderTraversal(root.leftChild);
		postOrderTraversal(root.rightChild);
		System.out.println(root.data);
	}
	
	/* Breadth First Search
	 * 
	 */
	public void breadthFirstSearch(Node root) {
		throw new NotImplementedException();
	}
	
	/* Depth First Search
	 * 
	 */
	public void depthFirstSearch(Node root) {
		throw new NotImplementedException();
	}
}
