/**
 * Created by Brandon Shreve on 8/9/2015.
 */
public class SortingAlgos {
    public void quickSort(int[] arr) {

    }

    public void mergeSort(int[] arrayToSort, int leftSide, int rightSide) {
        if (leftSide > rightSide) {
            // Find middle point, for dividing array into two
            int middle = 1+(leftSide+rightSide)/2;
            // mergeSort left side
            mergeSort(arrayToSort, leftSide, middle);
            // mergeSort right side
            mergeSort(arrayToSort, middle+1, rightSide);
            // merge both sorted halves into one sorted array
            merge(arrayToSort, leftSide, middle, rightSide);
        }
    }
    private void merge(int[] arrayToMerge, int leftSide, int middle, int rightSide) {
        int[] bufferArray = new int[arrayToMerge.length];

        for (int i = leftSide; i <= rightSide; i++) {
            bufferArray[i] = arrayToMerge[i];
        }

        int i = leftSide;
        int j = middle + 1;
        int k = leftSide;

        while (i <= middle && j <= rightSide) {
            if (bufferArray[i] <= bufferArray[j]) {
                arrayToMerge[k] = bufferArray[i];
                i++;
            }
            else {
                arrayToMerge[k] = bufferArray[j];
                j++;
            }
            k++;
        }

        while (i <= middle) {
            arrayToMerge[k] = bufferArray[i];
            k++;
            i++;
        }
    }
}
