/**
 * Created by Brandon Shreve on 7/27/2015.
 * Description: My implementation of a Hashtable
 */
public class MyHashtable {
    private String[] _htArray;
    private int _sizeOfArray;

    public MyHashtable(int size) {
        _sizeOfArray = size;
        _htArray = new String[_sizeOfArray];
    }

    public void putValueForKey(String key, String value) {
        _htArray[hashFunction(key)] = value;
    }

    public String getValueForKey(String key) {
        return _htArray[hashFunction(key)];
    }

    private int hashFunction(String key) {
        //TODO: This is a really dumb hash function, should probably refactor this.

        int sumOfAsciiVals = 0;

        for (char someChar : key.toCharArray()) {
            sumOfAsciiVals += (int)someChar;
        }
        return (sumOfAsciiVals + key.length()) % _sizeOfArray;
    }
}
