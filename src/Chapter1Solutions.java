import java.util.HashMap;

/**
 * Created by brandonshreve on 7/18/15.
 * Description: Each method is a a solution to a problem found in Chapter 1 of "Cracking the Coding Interview".
 */
public class Chapter1Solutions {

    /**
     * A method to determine if a string contains all unique characters. Making use of a HashMap.
     * @param someString
     * @return
     */
    public boolean chapter1Question1_DoesContainAllUniqueCharacters1(String someString) {
        HashMap<String, Integer> characterOccurences = new HashMap<>();

        for (int i = 0; i < someString.length(); i++) {
            char someChar = someString.charAt(i);
            if (characterOccurences.containsKey(someChar)) return false;
            characterOccurences.put(String.valueOf(someChar), 0);
        }
        return true;
    }

    /**
     * A method to determine if a string contains all unique characters. Making use of arrays and the ASCII table.
     * @param someString
     * @return
     */
    public boolean chapter1Question1_DoesContainAllUniqueCharacters2(String someString) {
        // If greater than 128, than there is a duplicate value somewhere.
        if (someString.length() > 128 ) return false;
        int[] asciiValOccurences = new int[128]; // 128 is the unextended ASCII table

        for (int i = 0; i < someString.length(); i++) {
            int asciiValOfChar = someString.charAt(i);
            if (asciiValOccurences[asciiValOfChar] == 0) return false;
            asciiValOccurences[asciiValOfChar] = 0;
        }
        return true;
    }

    /**
     * Takes two strings and determines if one string is a permutation of another.
     * @param stringOne
     * @param stringTwo
     * @return
     */
    public boolean chapter1Question2_IsOneStringAPermutationOfAnother1(String stringOne, String stringTwo) {
        if (stringOne.length() != stringTwo.length()) return false;
        HashMap<String, Integer> characterOccurences = new HashMap<>();

        // Loop through first string, counting the number of occurrences of each char
        for (char someChar : stringOne.toCharArray()) {
            Integer occurrences = characterOccurences.get(someChar) + 1;
            characterOccurences.put(String.valueOf(someChar), occurrences);
        }

        // Loop through second string, decrementing the number of occurrences. If a key doesn't exist or
        // the count dips below 0, then we know strings are not equal.
        for (char someChar : stringTwo.toCharArray()) {
            if (characterOccurences.get(someChar) == null) return false;
            Integer occurrences = characterOccurences.get(someChar) - 1;
            if ( occurrences < 0 ) return false;
            characterOccurences.put(String.valueOf(someChar), occurrences);
        }
        return true;
    }

    /***
     * Takes a String value and returns the same String value with all space values with an ascii encoded space value.
     * @param someString
     * @return
     */
    public String chapter1Question4_EncodeSpacesWithinString1(String someString) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char someChar : someString.toCharArray()) {
            if (someChar == ' ') {
                stringBuilder.append("%20");
                continue;
            }
            stringBuilder.append(String.valueOf(someChar));
        }
        return stringBuilder.toString();
    }

    /***
     * Takes a string and returns a compressed string (e.g., aabbaa is returned as a2b2a2).
     * @param someString
     * @return
     */
    public String chapter1Question5_CompressString1(String someString) {
        char previousChar = ' ';
        boolean firstIteration = true;
        int previousCharCount = 0;
        StringBuilder stringBuilder = new StringBuilder();

        for (char someChar : someString.toCharArray()) {
            if (someChar != previousChar) {
                if (!firstIteration) {
                    // Char changed, so append compressed chars
                    stringBuilder.append(previousChar);
                    stringBuilder.append(previousCharCount);
                }
                    // Continue to counting the next char
                    previousChar = someChar;
                    previousCharCount = 1;

                    continue;
                }
                firstIteration = false;
                ++previousCharCount;
            }
        return stringBuilder.toString();
    }
}