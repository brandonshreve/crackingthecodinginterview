/**
 * Created by Brandon Shreve on 8/11/2015.
 */
public class PascalsTriangle {

    public void pascalTriangle(int numOfRow) {
        for (int n = 0; n < numOfRow; n++) {
            for (int r = 0; r <= n; r++) {
                System.out.print(nCr(n, r) + " ");
            }
            System.out.println("");
        }
    }

    private int nCr(int n, int r) {
        int numerator = factorial(n);
        int denominator = factorial(r) * factorial(n - r);

        return numerator / denominator;
    }


    private int factorial(int n) {
        if (n == 0 || n == 1) return 1;
        return n*factorial(n-1);
    }
}
